# Architecture

## It should
1) Not suck much

2) Have some way of determining if an app is alive in a given cluster

3) Have multiple ways of prioritizing traffic. Per app based on multiple criteria (utilization?) Optionally also per cluster (initially based on an arbitrary priority but ideally based on cluster stats.)

4) Ideally allow individual customers to add their own entries instead of requiring an ops team

5) Be multi-tennant

6) Run in Kubernetes itself

7) Talk to the kube API directly so that end points can be added by selector instead of by IP/Name

8) Use as many existing technologies as possible instead of making up new crap

9) Be able to support secure updates (need to better define what that means.)

10) Be able to support OIDC authn/z?

11) Be crash only, no shutdown process

12) Be clustered, we aren't just storing dns entries so each node has to have the same state for it's data

13) Be able to be reconfigured on the fly while running

14) Support more elaborate health checks. Let's start with just an HTTP request which checks for content.

15) Have it's own health check

16) Return metrics in prometheus format

17) Support clusters in AWS and GKE (the two clouds I have immediate access to)

18) Deal with multiple formats for end point address. Google LB services only give IPs. AWS gives an R53 address other situations might do different things so how we figure out what the endpoint address is will need to be flexible.

19) Be administered via a RESTFull API

20) Have a swagger spec

## Additional nice to haves
1) Cluster probes. Some cluster based stats to use to steer traffic. I.E. this one is currently scaling up, send traffic to the other for a minute. (that example is probably stupid.)

2) Prioritize answers based on request. I.E. Topology based (geo,) prioritize to most local cluster (basically a type of geo,) Other?

3) Support split horizon. The simplest is by source IP but it would be nice to be able to configure horizon based on cluster and have KDNS automatically determine source (can that be done?)

4) Return the cluster internal address for within cluster requests and the external for others.

5) Have a command line interface

6) Have a UI

7) Have a terraform module (much later)

8) Have a config option that automatically generates DNS entries which correspond to kubernetes service discovery. I.E. everything available at <svc>.<namespace>.svc.cluster.local within the cluster is now available as <svc>.<namespace>.svc.<clustername>.<base domain>

## Possible other functionality
1) Misuse CH records like BIND does but for additional things. Bind supports the bind.version record.


## AuthZ
AuthN is fairly straight forward. We need to support external
authentication systems, OIDC initially.

AuthZ gets wierd. Ideally we don't have to keep our own authorization so
we don't have to duplicate anything. We just get the namespaces from k8s
and grant access based on those. But which roles, does everyone have to
use a subdomain named after their NS (I hate this idea.) Worst of all,
which cluster is authoritative?

I don't see any clean way to do this other than creating AuthZ records
within KDNS. I like most of the kubernetes role based approach as a model.
However it might be nice to see if we can make it a bit more clear.



# Other random thoughts on how to make this happen

KDNS is going to have to be multi-cluster itself since the whole purpose of it will be to
enable stability by being able to deploy to multiple clusters.

We need to determine how to synch data both within and between clusters.

Generally DNS servers are pretty small. Unless we screw up badly we won't need
to run a bunch in each cluster, the normal redundancy is more for stability than
performance. That being said we need to make sure we don't loose data if the node
we are on goes down or when a pod gets re-scheduled (guaranteed.)

We could do this with persistent data either by reading and writing to an s3 bucket
or with a PVC. Need to evaluate the benefits of both.

We could do the flexible gossip based clustering I've been working on for within
cluster. This is more work since we'll need to spend some time understanding
existing clustering libraries (probably raft?) and writing more stuff. On the
other hand this really strikes me as a more "Kubernetes" friendly architecture.

Need to see if it's possible (or even desired) to do the data transfer via DNS.
This actually seems unlikely. If we were just talking A/CNAME/ETC records that
would be easy but we will have a lot of other information like health check
configuration, label selectors and more.

Perhaps there's a config sync protocol and we are still able to leverage DNS
for state updates? Is there any reason to do this?

Config information will likely include credentials (or will it?) so that will
definately have to be encrypted.

State will change much more often than traditional DNS so the data synch between
clusters is pretty much just as important as within.

This will of course be written in golang because that's what I do these days.

Perhaps this is a full newer style service discovery tool which does return port information as well as IP. Gotta figure out how to do that via DNS ut we can return things via the aaPI as well. Perhaps svc records? but theres the whole _tcp|udp protocol segment of the svc path and i dont think folks want to have to kniw that ahead of time.

In addition to supporting clusters we have to be able to configure datacentes. Dc will contain clusters (and servers.) We also need to support availability zones and or regions(whatever the different clouds call those things) Probably we support a “public cloud” object which automatically configures those things? I probably cant automatically detect that for a given cloud but we can probably just have a list of regions/zones and you select those when you create the cloud object

Harder question. How do we hanlde companies with multiple accounts like Nord. This might only be an issue if we are going to support native cloud objects as well as clusters. So early on probably we just allow the configuration of multiple cloud instances.

Don’t boil the ocean. This is a GSLB product not a kube replacement.

For “server” registration. See if I can get a pods hostname from $hostname. Then resolve that to an IP from another pod. If not confirm that statefulset+HPA actually works now (supposedly supported since 1.9) It would be nice to use a deployment instead of a statefulset just because some other 3rd party stuff (and k8s administrators) forget about statefulsets. That being said if we are ever going to look at deployments/replicasets/statefulsets it might be worth it to use one of those simply so that we screw ourselves up if we forget.

For config storage it might be nice to update a configmap.  However we will probably want state to be part of that storage which probably means making updates more often than would be nice with the CM. It DEFINAYELY means our updates have to be more granular than simply synching the whole DB (though that could probably also happen periodically as a backup.) need to come up with a way to determine a diff between full configs like a hash of the whole DB. Maybe take a lesson from the event stream based architecture on this one?

For config storage one thought is to create some CRDs and update those the way ark does. However if we want to be able to be run by small teams who don’t admin their own clusters that might not be a good choice.


# Resources
Here are a few useful references from other folks writing DNS servers in golang.

In order from simple to complete

1) [A very basic DNS server](https://gist.github.com/walm/0d67b4fb2d5daf3edd4fad3e13b162cb)
   - A 57 line go program with the records static in the code. It uses the:
2) [the miekg go DNS library](https://github.com/miekg/dns)
   - Which is also used by:
3) [the coredns project](https://github.com/coredns/coredns)
   - Which is now part of kubernetes.

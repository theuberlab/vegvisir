PROJECT_ROOT := $(shell git rev-parse --show-toplevel)
include $(PROJECT_ROOT)/Makefile.variables
# Vars
# This is the home of dynamically defined variables, variable paths etc.
# All other vars should live in Makefile.variables.
PKGS := $(shell go list ./... | grep -v /vendor)
BIN_DIR := $(GOPATH)/bin
GOMETALINTER := $(BIN_DIR)/gometalinter
VERSION ?= $(MAIN_VERSION)
BUILD ?= `date -u +build-%Y%m%d.%H%M%S` # Not actually using this yet but I want to.
os = $(word 1, $@)
currOS := $(shell uname | tr '[:upper:]' '[:lower:]')
BUILDDIR := $(PROJECT_ROOT)/build
# Temporarily using this until we are publishing automatially. #TODO: This should _probably_ be under BUILDDIR if we keep it.
OUTPUTDIR := $(PROJECT_ROOT)/release

# This works to only re-compile the binary Vegvísir if the file main.go when the target is a constant.
# I want to continue to do that so I will have to do a little dance. Perhaps version doesn't include the second, only the day or a hash or something.
# Then I will have to make the target be the file name instead of just the platform.
$(currOS): pkg/main.go
	@echo "Made for $(currOS)"
#	GOOS=darwin GOARCH=amd64 go build -ldflags "-X main.Version=$(VERSION)"

pkg/main.go:


#TODO: Update these to accept a context as well.
.PHONY: deploy
deploy:
	@echo "Deploying"
	if [[ "$(namespace)" == "" ]]; then          \
	    read -ep "Target namespace: " namespace; \
	fi;                                          \
	if [[ "$${namespace}" != "" ]]; then         \
	    kubectl -n $${namespace} apply           \
	        --filename k8s-resources             \
	        --record;                            \
    fi

.PHONY: teardown
teardown:
	@echo "Tearing Down"
	if [[ "$(namespace)" == "" ]]; then                   \
	    read -ep "Target namespace: " namespace;          \
	fi;                                                   \
	if [[ "$${namespace}" != "" ]]; then                  \
	    kubectl -n $${namespace} delete -f k8s-resources; \
	fi

#TODO: Make this work with the new binary file name and directory.
serve: $(currOS)
	$(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64

$(BUILDDIR):
	mkdir $(BUILDDIR)

$(OUTPUTDIR):
	mkdir $(OUTPUTDIR)

.PHONY: clean
clean:
	rm $(BUILDDIR)/*
	rm $(OUTPUTDIR)/*

.PHONY: test
test: lint
	go test $(PKGS)

$(GOMETALINTER):
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install &> /dev/null

.PHONY: lint
lint: $(GOMETALINTER)
	gometalinter ./... --vendor

#.PHONY: $(PLATFORMS)
$(PLATFORMS): $(BUILDDIR) pkg/main.go
	GOOS=$(os) GOARCH=amd64 go build -ldflags "-X main.Version=$(VERSION)" -o $(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(os)-amd64 ./pkg

.PHONY: release
release: windows linux darwin

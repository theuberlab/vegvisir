# Stories

TODO: Expand these to answer the following questions:
* How do they want to manage the records (I.E. manually, via automation, through integration?)
* Requires multi-tennancy?
* How well developed is their CI/CD process/infrastructure (This overlaps with the first item, may need to be rewritten.)


1) Elijah is the team lead at an organization with an international footprint
who wishes to direct client traffic to kubernetes clusters closer to
their clients to reduce application latency.

2) Malik is an application owner working at a large organization with a
mandate to be multi-cloud.

3) Denise is an administrator managing Kubernetes clusters for a fast-paced, medium sized
business looking for a way to migrate workloads between kubernetes
clusters in order to perform upgrades and migrations.

4) Paul is a CIO working for a large organization attempting to reduce
costs by shifting workloads between multiple clouds and taking advantage
of lower traffic tiers at each provider.

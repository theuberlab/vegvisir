# About

This is where I will perform product management tasks like user stories and more.

# Issue Process.

Issues are tracked on the Trello board.
Issues come in to the project in one of two ways.
1) They are created by project members. These are created within trello directly and should go to the proposed board.
2) They are submitted by other users. These will likely be created in issues where non-project users have access. These should be reviewed in a timely fashion and if they are worthy of discussion moved to trello's proposed board.

# [User Stories](UserStories.md)

# Timeline
Which is something like a roadmap but WAY more vague.

Ability to serve traditional DNS A records. - Done

Ability to serve the DNS name of a kubernetes service type load balancer
which is hosted in AWS. - Done

Ability to serve multiple records as RR

Support Authorization (not authentication. This is so that development occurs with authz already in place.)

Support RBAC. Do something similar to k8s with roles and rolebindings and verbs

Swagger






Command line tool



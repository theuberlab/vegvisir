package vegvisir_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	_ "bitbucket.org/theuberlab/vegvisir/tests/e2eTests"
)

func TestVegvisir(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Vegvisir E2e Test Suite")
}

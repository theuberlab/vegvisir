package e2eTests

import (
	"bitbucket.org/theuberlab/vegvisir/pkg/TradDNS"
	"fmt"

	g "github.com/onsi/ginkgo"
	o "github.com/onsi/gomega"
)

var (
	//// Byte arrays to store the private key and certificate we will use to serve TLS
	//keyPEM 		[]byte
	//certPEM 	[]byte
	//gdns		*GDNS.GDNS
	tradDNS		*TradDNS.TradDNS
	//kubeAPI		*KubeAPI.KubeAPI
	TheZone		= &TradDNS.Zone{
		Origin:				"example.com.",
		TTL:				3600,
		SOA:				"ns1.example.com",
		Hostmaster:			"hostmaster.example.com",
		SerialNumber:		0,
		TimeToRefresh:		3600,
		TimeToRetry:		3600,
		TimeToExpire:		3600,
		MinimumTTL:			3600,
	}
	//configFile              string
	//configContext           string
	//logLevel                string
	//logFormat               string
	//logUTC                  bool
	//apiport					int
	//dnsport					int
	//appVersion				string
	//client					*k8s.Client
	//ctx						context.Context
	//metricsArray			*Metrics.VerifyMetrics
)


var _ = g.Describe("[Feature:withconfig][CI]", func() {

	g.Describe("When queried for A records", func() {
		g.BeforeEach(func() {
			//func createDefaultTradDNSRecords() {
			tradDNS.DNSRecords["www.example.com."] = &TradDNS.ResourceRecord{
				Zone:			TheZone,
				HostLabel:		"www",
				TTL:			0,
				RecordClass:	TradDNS.IN,
				RecordType:		TradDNS.A,
				RecordData:		"10.1.2.3",
			}

			tradDNS.DNSRecords["email.example.com."] = &TradDNS.ResourceRecord{
				Zone:			TheZone,
				HostLabel:		"email",
				TTL:			0,
				RecordClass:	TradDNS.IN,
				RecordType:		TradDNS.A,
				RecordData:		"10.1.2.4",
			}
			//}
		})

		g.It(fmt.Sprintf("Should return correct results for www.example.com."), func() {
			o.Expect(tradDNS.DNSRecords["www.example.com."].RecordData).To(o.Equal("10.1.2.3"))
		})

		g.It(fmt.Sprintf("Should return correct results for email.example.com."), func() {
			o.Expect(tradDNS.DNSRecords["email.example.com."].RecordData).To(o.Equal("10.1.2.4"))
		})

	})
})
# Vegvísir
The new generation of service discovery is the same as the old generation of service discovery.

Vegvísir (or vegvisir) is a DNS product with two goals.

## Modern Service Discovery Using DNS.
There are a lot of new products being created for service discovery.
The bulk of what they do and are used for can already be handled by existing
DNS technologies however many people are unaware of this because
"pure" DNS implimentations are not commonly configured this way.

The main ignored solutions are [dynamic DNS](https://en.wikipedia.org/wiki/Dynamic_DNS)
and multi-master DNS servers which are not linked because they are implementation specific.

Microsoft has gotten a lot of mileage out of combining these two technologies
with a few other standards when they created [Active Directory](https://en.wikipedia.org/wiki/Active_Directory)
which is capable of numerous masters, handles geographic and logical sharding and a number of other solutions.

That being said there are still a few very real reasons why organizations
choose to create something new instead of relying on DNS. In very high
churn environments such as serverless or container orchestration (I.E. Kubernetes)
where an instances lifecycle is frequently measured in minutes or seconds
instead of months or years updates can become an issue.
For example [the existing notify mechanism](https://tools.ietf.org/html/rfc1996)
does not include any update details. It simply notifies a down stream
server that it should check for an updated zone.

Vegvísir will be a DNS server which addresses these issues and is
suitable for modern, high churn, high traffic environments.
To some degree this will require extensions to the current DNS protocol.
Rather than implement these in completely unrelated and proprietary side
channels the intent is to propose new RFCs which extend DNS.

Vegvísir will be the working example of these proposals.
These will be discussed [in the architecture documentation](Architecture.md)


## Kubernetes Aware GSLB
The second goal of Vegvísir would be to produce a GSLB product which
will not only serve manually configured results but be able to interact
with systems such as Kubernetes and both serve results discoverd from Kubernetes
as well as understand the clusters in order to return more appropriate results
when requests are made within Kubernetes clusters.


# Status
The current version of Vegvísir works to a very limited degree.

## Static (Traditional DNS) Records.
It is able to serve statically configured (via an API call) A records

## GSLB
It is able to serve statically configured GSLB records and return different
 IP addresses based on the IP of the originating request.

## Kubernetes
It is able to serve A records or CNAMES for kubernetes services.

It does this simply by returning the loadBalancer.ingress.hostname for
the service. Which really makes it suitable only for requests which originate
outside of the kubernetes cluster.

## API
By default the API server listens on port 8443 and the DNS server
listens on port 4353.

Vegvísir will serve a GSLB record (which includes kubernetes) if it
exists. If not it will fall through to traditional DNS before returning an NXDOMAIN.


Records are managed through the API. There are three primary interfaces

/dns for managing static DNS records.
/k8s for managing Kubernetes Servers.
/gdns for managing GSLB records.

Once kubernetes servers are added you must call /k8s-update to create
the internal api server client objects. Yes this is stupid and will be removed.

Example objects of each type are created at startup to determine how
to create these objects fetch one of the example objects.


# Building
Currently to build the project simply use make which will build for the current OS.
```make```

There are some targets in there for building multiple OSes as well as deploying to kubernetes but I doubt they all work.

# Contributing
Issues, comments and pull requests are welcome.


# Architecture
[Architecture, which currently means "most of my notes," is over here in it's own file.](Architecture.md)

Vegvísir is copywrite Aaron Forster and The Uberlab.
It is usable under the Creative Commons [BY-ND](https://creativecommons.org/licenses/by-nd/4.0/) license.



# [Product Management](ProductManagement/README.md)

Let's have some sort of actual plan.

Product management stuff will be done [in the product management section of the repo.](ProductManagement/README.md)


# The Name
Vegvisir (Vegvísir) is Icelanding spell. [According to wikipedia the name
means sign post or wayfinder.](https://en.wikipedia.org/wiki/Vegv%C3%ADsir)

It is a symbol which if carried upon one's person would prevent them from
loosing their way.

https://norse-mythology.org/vegvisir/

package GDNS

import (
	"github.com/ericchiang/k8s"
	"bitbucket.org/theuberlab/vegvisir/pkg/TradDNS"
)

type GdnsRecord struct {
	Name		string
	Services	[]ServiceAddress
	//Service		ServiceAddress
	Zone		*TradDNS.Zone	// The zone which contains this record.
}

// The internal model of a kubernetes service.
type ServiceAddress struct {
	Name				string
	InternalEndpoint	string
	ExternalEndpoint	string
	Namespace			string
	Selector			Selector	// The selector used to find the service.
	Cluster 			string	// The cluster upon which this service is hosted.
	Status				bool	// Is it currently available?
}

// Since k8s.LabelSelector has no public fields we can't use it as a type so we have to create our own to be able to encode and decode it automatically
type Selector struct {
	Method		string
	Key			string
	Value		string
}

func GetK8sSelector(selector Selector) (*k8s.LabelSelector) {
	k8sSelector := new(k8s.LabelSelector)

	switch selector.Method {
	case "Eq":
		k8sSelector.Eq(selector.Key, selector.Value)
	case "In":
		k8sSelector.In(selector.Key, selector.Value)
	case "NotEq":
		k8sSelector.NotEq(selector.Key, selector.Value)
	case "NotIn":
		k8sSelector.NotIn(selector.Key, selector.Value)
	}

	return k8sSelector
}
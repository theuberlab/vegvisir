package GDNS

// ######################################################################################
// # GlobalDNS Record Management
// ######################################################################################

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/theuberlab/vegvisir/pkg/KubeAPI"
	"bitbucket.org/theuberlab/vegvisir/pkg/lug"
	"github.com/gorilla/mux"

	corev1 "github.com/ericchiang/k8s/apis/core/v1"

)

type GDNS struct {
	ctx						context.Context
	GDNSRecords				map[string]*GdnsRecord
	kubeAPIInterface		*KubeAPI.KubeAPI
}

// Returns a pointer to an initialized GDNS Object
func NewGDNS(ctx context.Context, kubeAPI *KubeAPI.KubeAPI) *GDNS {
	return &GDNS{
		ctx: 				ctx,
		GDNSRecords:		make(map[string]*GdnsRecord),
		kubeAPIInterface:	kubeAPI,
	}
}

// The API way to fetch all Records.
func (g *GDNS) GetGDNSRecords(w http.ResponseWriter, r *http.Request) {
	lug.LogDebug("Message", "Request for all GDNS records.")
	json.NewEncoder(w).Encode(g.GDNSRecords)
}

// The API way to fetch a Record.
func (g *GDNS) GetGDNSRecord(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	lug.LogDebug("Message", "Request for GDNS record.", "RecordName", params["id"])
	json.NewEncoder(w).Encode(g.GDNSRecords[params["id"]])
}

// Adds a new Record.
func (g *GDNS) CreateGDNSRecord(w http.ResponseWriter, r *http.Request) {
	lug.LogDebug("Message", "CreateDNSRecord called")
	var record GdnsRecord

	err := json.NewDecoder(r.Body).Decode(&record)

	if err != nil {
		lug.LogError("Message", "Error decoding request body")
		http.Error(w, err.Error(), 400)
		json.NewEncoder(w).Encode("{ \"Message\": \"Error decoding request body\"")
		return
	}

	newRecord, _ := json.Marshal(record)

	lug.LogDebug("Message", "Creating GDNS record.", "RecordName", record.Name, "Record", newRecord)


	g.GetExternalAddress(&record)

	g.GDNSRecords[record.Name] = &record

	json.NewEncoder(w).Encode(g.GDNSRecords)
}

// Delete's a record.
func (g *GDNS) DeleteGDNSRecord(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	lug.LogDebug("Message", "Deleting GDNS record.", "RecordName", params["id"])

	delete(g.GDNSRecords, params["id"])
	json.NewEncoder(w).Encode(g.GDNSRecords)
}


// Fetch the addresses neccessary
func (g *GDNS) GetExternalAddress(record *GdnsRecord) {

	lug.LogDebug("Message", "Fetching External Addresses for service.")

	var services corev1.ServiceList
	//var service corev1.Service

	fmt.Printf("Fetching services with selector\n")

	for idx, svc := range record.Services {
		slct := GetK8sSelector(svc.Selector)
		lug.LogDebug("Message", "Created Selector for cluster", "Selector", slct.String(), "Cluster", fmt.Sprintf("[%s]", svc.Cluster))

		lug.LogDebug("Message", "Cluster selected.", "Cluster", g.kubeAPIInterface.APIServers[svc.Cluster].Name, "URL", g.kubeAPIInterface.APIServers[svc.Cluster].URL)

		lug.LogDebug("Message", "Checking servers")
		for key, value := range g.kubeAPIInterface.APIServers {
			lug.LogDebug("Message", "API Server", "key", fmt.Sprintf("[%s]", key), "URL", fmt.Sprintf("[%s]", value.URL))
		}

		g.kubeAPIInterface.APIServers[svc.Cluster].Client.List(g.ctx, svc.Namespace, &services, slct.Selector())

		lug.LogDebug("Message", "Fetching services for selector", "Selector", fmt.Sprintf("%v", svc.Selector))

		lug.LogDebug("Message", "Found services", "NumResults", len(services.Items))

		for _, svc := range services.Items {
			//lug.LogDebug("Message", "Found service", "ServiceName", *svc.Metadata.Name, "Service", fmt.Sprintf("%v", svc))

			//TODO: In the future we are going to have to deal with a whole bunch of other possible address locations.
			lug.LogDebug("Message", "Found service", "ClusterIP", *svc.Spec.ClusterIP, "LBIps", *svc.Spec.LoadBalancerIP, "ExternalIPs", fmt.Sprintf("%v", svc.Spec.ExternalIPs), "ExternalName", *svc.Spec.ExternalName, "LoadBalancer", fmt.Sprintf("%v", svc.Status.LoadBalancer))
		}

		//TODO: Throw an error if we get more than one service. Or decide that it's o.k. to reference more than one.


		loadBalancer := services.Items[0].Status.LoadBalancer


		record.Services[idx].ExternalEndpoint = loadBalancer.Ingress[0].GetHostname()

		record.Services[idx].InternalEndpoint = services.Items[0].Spec.GetClusterIP()

	}

	for index, svc := range services.Items {
		lug.LogDebug("Message", "Found service", "ServiceName", *svc.Metadata.Name, "Index", index)
	}
}

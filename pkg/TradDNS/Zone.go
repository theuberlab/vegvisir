package TradDNS

// A Zone is the main container of DNS. It is also coloquially known as a DNS domain. It holds records.

//TODO: Convert Hostmaster to an actual email address format.
type Zone struct {
	Origin			string	// Origin is the name of the zone. In a BIND file it's what gets appended to any non fully qualified (ends in a .) record.
	TTL				int		// The default time clients should cache results. This can be overridden in an individual record.
	SOA				string	// Start of Authority. The nameserver that contains the original zone file and not an AXFR transferred copy.
	Hostmaster      string	// Address of the party responsible for the zone. A period “.” is used in place of an “@” symbol. For email addresses that contain a period, this will be escaped with a slash “/”.
	SerialNumber	int		// Version number of the zone. As you make changes to your zone file, the serial number will increase.
	TimeToRefresh	int		// How long in seconds a nameserver should wait prior to checking for a Serial Number increase within the primary zone file. An increased Serial Number means a transfer is needed to sync your records. Only applies to zones using secondary DNS.
	TimeToRetry		int		// How long in seconds a nameserver should wait prior to retrying to update a zone after a failed attempt. Only applies to zones using secondary DNS.
	TimeToExpire	int		// How long in seconds a nameserver should wait prior to considering data from a secondary zone invalid and stop answering queries for that zone. Only applies to zones using secondary DNS.
	MinimumTTL		int		// Also known as NegTTL. How long in seconds that a nameserver or resolver should cache a negative response.
}



// This is what a zonefile looks like. We should include the zone level stuff like
// TTL, address, etc.
//
//$ORIGIN example.com.
//@                      3600 SOA   ns1.p30.dynect.net. (
//zone-admin.dyndns.com.     ; address of responsible party
//2016072701                 ; serial number
//3600                       ; refresh period
//600                        ; retry period
//604800                     ; expire time
//1800                     ) ; minimum ttl
//86400 NS    ns1.p30.dynect.net.
//86400 NS    ns2.p30.dynect.net.
//86400 NS    ns3.p30.dynect.net.
//86400 NS    ns4.p30.dynect.net.
//3600 MX    10 mail.example.com.
//3600 MX    20 vpn.example.com.
//3600 MX    30 mail.example.com.
//60 A     204.13.248.106
//3600 TXT   "v=spf1 includespf.dynect.net ~all"
//mail                  14400 A     204.13.248.106
//vpn                      60 A     216.146.45.240
//webapp                   60 A     216.146.46.10
//webapp                   60 A     216.146.46.11
//www                   43200 CNAME example.com.
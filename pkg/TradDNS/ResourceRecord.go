package TradDNS

// Everything in a DNS zone that isn't a global setting for the zone (like the TTL) is a resource record. Each RR
// has a type such as A or CNAME which allows clients to ask for them and defines the format of the response.
type ResourceRecord struct {
	// This is how it's done in a BIND file. We might be able to get some performance improvements later by creating individual objects for different RR types which store their data in more proper formats instead of storing everything as a string.
	Zone		*Zone	// The zone which contains this record.
	HostLabel	string	// A host label helps to define the hostname of a record and whether the $ORIGIN hostname will be appended to the label. Fully qualified hostnames terminated by a period will not append the origin.
	TTL			int		// TTL is the amount of time in seconds that a DNS record will be cached by an outside DNS server or resolver.
	RecordClass RRClass	// There are three classes of DNS records: IN (Internet), CH (Chaosnet), and HS (Hesiod). The IN class will be used for the Managed DNS service.
	RecordType	RRType	// Where the format of a record is defined.
	RecordData	string	// The data within a DNS answer, such as an IP address, hostname, or other information. Different record types will contain different types of record data.
}

// This one is a little un-neccessary.
type RRClass int

const (
	IN    RRClass = 1	// The Internet
	CS    RRClass = 2	// The CSNET class (Obsolete - used only for examples in some obsolete RFCs) Provided email lookups
	CH    RRClass = 3	// The CHAOSnet class. (Obsolete
	HS    RRClass = 4	// Hesiod basically DNS based password files. https://en.wikipedia.org/wiki/Hesiod_(name_service)
)

// This is the bit that matters.
type RRType	int

// From https://tools.ietf.org/html/rfc1035
const (
	A		RRType = 1	// A host address
	NS		RRType = 2	// An authoritative name server
	MD		RRType = 3	// A mail destination (Obsolete - use MX)
	MF		RRType = 4	// A mail forwarder (Obsolete - use MX)
	CNAME	RRType = 5	// The canonical name for an alias
	SOA		RRType = 6	// Marks the start of a zone of authority
	MB		RRType = 7	// A mailbox domain name (EXPERIMENTAL)
	MG		RRType = 8	// A mail group member (EXPERIMENTAL)
	MR		RRType = 9	// A mail rename domain name (EXPERIMENTAL)
	NULL	RRType = 10	// A null RR (EXPERIMENTAL)
	WKS		RRType = 11	// A well known service description
	PTR		RRType = 12	// A domain name pointer
	HINFO	RRType = 13	// Host information
	MINFO	RRType = 14	// Mailbox or mail list information
	MX		RRType = 15	// Mail exchange
	TXT		RRType = 16	// Text strings
)
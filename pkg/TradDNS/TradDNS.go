package TradDNS

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/theuberlab/vegvisir/pkg/lug"
	"github.com/gorilla/mux"
)

type TradDNS struct {
	DNSRecords		map[string]*ResourceRecord
}

// Returns a pointer to an initialized GDNS Object
func NewTradDNS() *TradDNS {
	return &TradDNS{
		DNSRecords: make(map[string]*ResourceRecord),
	}
}

// The API way to fetch all Resource Records.
func (t *TradDNS) GetDNSRecords(w http.ResponseWriter, r *http.Request) {
	lug.LogDebug("Message", "Request for all DNS records.")
	json.NewEncoder(w).Encode(t.DNSRecords)
}

// The API way to fetch a Resource Record.
func (t *TradDNS) GetDNSRecord(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	lug.LogDebug("Message", "Request for DNS record.", "RecordName", params["id"])
	json.NewEncoder(w).Encode(t.DNSRecords[params["id"]])
}

// Adds a new Resource Record.
func (t *TradDNS) CreateDNSRecord(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	lug.LogDebug("Message", "Creating DNS record.", "RecordName", params["id"])
	var record ResourceRecord
	_ = json.NewDecoder(r.Body).Decode(&record)
	record.HostLabel = params["id"]
	t.DNSRecords[params["id"]] = &record
	json.NewEncoder(w).Encode(t.DNSRecords)
}

// Delete's a resource record.
func (t *TradDNS) DeleteDNSRecord(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	lug.LogDebug("Message", "Deleting DNS record.", "RecordName", params["id"])
	delete(t.DNSRecords, params["id"])
	json.NewEncoder(w).Encode(t.DNSRecords)
}

package lug

import (
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

var (
	logger			log.Logger

	LogError func(keyvals ...interface{}) error
	LogWarn  func(keyvals ...interface{}) error
	LogInfo  func(keyvals ...interface{}) error
	LogDebug func(keyvals ...interface{}) error

	logUTC			bool
	loglevel		string
	logFormat		string
)

func InitLogger(logFormat string, logLevel string, logUTC bool) {
	switch logFormat {
	case "logfmt":
		logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	case "json":
		logger = log.NewJSONLogger(log.NewSyncWriter(os.Stderr))
	}

	// Set the log level
	switch logLevel {
	case "None":
		logger = level.NewFilter(logger, level.AllowNone())
	case "Error":
		logger = level.NewFilter(logger, level.AllowError())
	case "Warn":
		logger = level.NewFilter(logger, level.AllowWarn())
	case "Info":
		logger = level.NewFilter(logger, level.AllowInfo())
	case "Debug":
		logger = level.NewFilter(logger, level.AllowDebug())
	case "All":
		logger = level.NewFilter(logger, level.AllowAll())
	}

	logTimeFormat := log.DefaultTimestamp

	if logUTC {
		logTimeFormat = log.DefaultTimestampUTC
	}

	// Set some default values which will appear in all log messages such as the timestamp. Caller lvl 4 shows whatever
	// called one of the below created functions. I.E. where in main we called alog.LogError.
	logger = log.With(logger, "ts", logTimeFormat, "caller", log.Caller(4))

	// Create some aliases for log levels
	LogError = level.Error(logger).Log
	LogWarn = level.Warn(logger).Log
	LogInfo = level.Info(logger).Log
	LogDebug = level.Debug(logger).Log

	LogWarn("Message", "Initialized lug", "LogLevel", loglevel, "LogFormat", logFormat, "LogUTC", logUTC)
}
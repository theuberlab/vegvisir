package KubeAPI

// ######################################################################################
// # K8s Server Management
// ######################################################################################

import (
	"context"
	"encoding/json"
	"net/http"

	"bitbucket.org/theuberlab/vegvisir/pkg/lug"
	"github.com/gorilla/mux"
)

type KubeAPI struct {
	APIServers		map[string]*KubeAPIServer
}

// Returns a pointer to an initialized GDNS Object
func NewKubeAPI() *KubeAPI {
	return &KubeAPI{
		APIServers: make(map[string]*KubeAPIServer),
	}
}

// The API way to fetch all Cluster Records.
func (k *KubeAPI) GetK8SRecords(w http.ResponseWriter, r *http.Request) {
	lug.LogDebug("Message", "Request for all cluster records.")
	json.NewEncoder(w).Encode(k.APIServers)
}

// The API way to fetch a Cluster Record.
func (k *KubeAPI) GetK8SRecord(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	lug.LogDebug("Message", "Request for cluster record.", "ClusterName", params["id"])
	json.NewEncoder(w).Encode(k.APIServers[params["id"]])
}

// Adds a new Cluster Record.
func (k *KubeAPI) CreateK8SRecord(w http.ResponseWriter, r *http.Request) {
	var record KubeAPIServer
	_ = json.NewDecoder(r.Body).Decode(&record)
	k8sServer, _ := json.Marshal(record)

	lug.LogDebug("Message", "Creating cluster record.", "ClusterName", record.Name, "K8SServer", k8sServer)
	//record.Name = params["id"]
	k.APIServers[record.Name] = &record
	json.NewEncoder(w).Encode(k.APIServers)
}

// Delete's a Cluster record.
func (k *KubeAPI) DeleteK8SRecord(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	lug.LogDebug("Message", "Deleting cluster record.", "ClusterName", params["id"])

	delete(k.APIServers, params["id"])
	json.NewEncoder(w).Encode(k.APIServers)
}

func (k *KubeAPI) UpdateClusters(ctx context.Context) {
	lug.LogWarn("Message", "Checking clusters")

	for _, apiServer := range k.APIServers {

		apiServer.MakeClient()
		//someClient := apiServer.MakeClient()

		lug.LogWarn("Message", "Fetching Nodes from cluster", "Cluster", apiServer.Name)

		nodes := apiServer.FetchAllNodes(ctx)

		for _, node := range nodes.Items {
			lug.LogWarn("Message", "Found node %s", *node.Metadata.Name)
			//lug.LogWarn("NodeName", *node.Metadata.Name, "Schedulable", !*node.Spec.Unschedulable)
		}
	}
}


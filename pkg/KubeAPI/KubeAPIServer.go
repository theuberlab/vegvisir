package KubeAPI

import (
	"context"
	"encoding/base64"
	"fmt"
	"os"

	"github.com/ericchiang/k8s"
	corev1 "github.com/ericchiang/k8s/apis/core/v1"
	//metav1 "github.com/ericchiang/k8s/apis/meta/v1"
)


// Represents a kubernetes API server
type KubeAPIServer struct {
	Name		string
	URL			string
	CAPem		string
	AuthToken	string
	ClientName	string
	Client		*k8s.Client
}

//TODO: Figure out how to detect when I have broken credentials instead of throwing a panick when one of these activities fails.
// Fetches all Nodes. Just a dumb thing to prove we can talk to kubernetes
func (srv *KubeAPIServer) FetchAllNodes(ctx context.Context) (corev1.NodeList) {
	var nodes corev1.NodeList

	fmt.Printf("Fetching nodes")

	err := srv.Client.List(ctx, "", &nodes)

	if err != nil {
		fmt.Printf("Unable to fetch nodes. Error: %v\n", err)
		os.Exit(1)
	}

	return nodes
}

func (srv *KubeAPIServer) FetchService(ctx context.Context) (corev1.Service) {
	var services corev1.ServiceList
	var service corev1.Service

	fmt.Printf("Fetching services\n")

	err := srv.Client.List(ctx,"", &services)

	if err != nil {
		fmt.Printf("Unable to fetch services. Error: %v\n", err)
		os.Exit(1)
	}

	for _, svc := range services.Items {
		fmt.Printf("Message: Found node Service: %s\n", *svc.Metadata.Name)
		//logWarn("NodeName", *node.Metadata.Name, "Schedulable", !*node.Spec.Unschedulable)
	}

	return service
}

// Create a client which includes all the current API Servers.
func (srv *KubeAPIServer) MakeClient() (*k8s.Client) {
	var thisClient *k8s.Client
	var config k8s.Config
	var err error

	var cluster k8s.Cluster

	clusterUser := fmt.Sprintf("%s_user", srv.Name)

	config.Kind = "Config"

	var CertAuthorityData []byte
	CertAuthorityData, _ = base64.StdEncoding.DecodeString(srv.CAPem)

	cluster.Server = srv.URL
	cluster.CertificateAuthorityData = CertAuthorityData

	cluster.InsecureSkipTLSVerify = true

	config.Clusters = append(config.Clusters, k8s.NamedCluster{
		Name: srv.Name,
		Cluster: cluster,
	})

	var prodUser k8s.AuthInfo

	prodUser.Token = srv.AuthToken

	config.AuthInfos = append(config.AuthInfos, k8s.NamedAuthInfo{
		Name: clusterUser,
		AuthInfo: prodUser,
	})

	var cluster1Context k8s.Context

	cluster1Context.Cluster = srv.Name
	cluster1Context.AuthInfo = clusterUser

	config.Contexts = append(config.Contexts, k8s.NamedContext{
		Name: srv.Name,
		Context: cluster1Context,
	})

	config.CurrentContext = srv.Name

	thisClient, err = k8s.NewClient(&config)

	if (err != nil) {
		fmt.Printf("Message: %s Error %v", "Failed to create client", err)
	}

	srv.ClientName = config.CurrentContext

	srv.Client = thisClient

	return thisClient
}
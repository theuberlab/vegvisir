package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/theuberlab/vegvisir/pkg/GDNS"
	"bitbucket.org/theuberlab/vegvisir/pkg/KubeAPI"
	"bitbucket.org/theuberlab/vegvisir/pkg/Metrics"
	"bitbucket.org/theuberlab/vegvisir/pkg/TradDNS"
	"bitbucket.org/theuberlab/vegvisir/pkg/lug"
	"github.com/ericchiang/k8s"
	"github.com/gorilla/mux"
	"github.com/miekg/dns"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	// Byte arrays to store the private key and certificate we will use to serve TLS
	keyPEM 		[]byte
	certPEM 	[]byte
	gdns		*GDNS.GDNS
	tradDNS		*TradDNS.TradDNS
	kubeAPI		*KubeAPI.KubeAPI
	TheZone		= &TradDNS.Zone{
		Origin:				"example.com.",
		TTL:				3600,
		SOA:				"ns1.example.com",
		Hostmaster:			"hostmaster.example.com",
		SerialNumber:		0,
		TimeToRefresh:		3600,
		TimeToRetry:		3600,
		TimeToExpire:		3600,
		MinimumTTL:			3600,
	}
	configFile              string
	configContext           string
	logLevel                string
	logFormat               string
	logUTC                  bool
	apiport					int
	dnsport					int
	appVersion				string
	client					*k8s.Client
	ctx						context.Context
	metricsArray			*Metrics.VerifyMetrics
)

// Variables that will be used as handles for specific activities.
var (
	// Creates the top level context for all commands flags and arguments
	app      			= kingpin.New("podstartupwatcher", "Connects to the kubernetes API and watches pod events while it spins up and destroys pods.")
	versionInfo			*VersionInfo
)

// I don't really understand how this bit works but it's part of some cross-site scripting protection I got from a friend.
var corsHeaders = map[string]string{
	"Access-Control-Allow-Headers":  "Accept, Authorization, Content-Type, Origin",
	"Access-Control-Allow-Methods":  "GET, OPTIONS",
	"Access-Control-Allow-Origin":   "*",
	"Access-Control-Expose-Headers": "Date",
}

// Initialize some components that need to be available early on.
func init() {
	versionInfo = &VersionInfo{} // Doing this weird thing due to some legacy reasons. Should probably drop it.
	appVersion = versionInfo.GetVersion()

	// Setup Kingpin flags
	// Set the application version number
	app.Version(appVersion)
	// Allow -h as well as --help
	app.HelpFlag.Short('h')

	app.Flag("apiport", "The port the API server should listen on. Can be set via VGVSR_API_PORT.").Short('A').Default("8443").Envar("VGVSR_API_PORT").IntVar(&apiport)
	app.Flag("dnsport", "The port the DNS server should listen on. Can be set via VGVSR_DNS_PORT.").Short('D').Default("4353").Envar("VGVSR_DNS_PORT").IntVar(&dnsport)

	// Logging flags
	app.Flag("log-utc", "Timestamp lug.Log messages in UTC instead of local time. Can be set via VGVSR_LOG_UTC").Default("false").Envar("VGVSR_LOG_UTC").BoolVar(&logUTC)
	app.Flag("logLevel", "The level of lug.Logging to use. Can be set via VGVSR_LOG_LEVEL").Short('l').Default("Error").Envar("VGVSR_LOG_LEVEL").EnumVar(&logLevel, "None",
		"Error",
		"Warn",
		"Info",
		"Debug",
		"All")
	app.Flag("logformat", "What output format to use. Can be set via VGVSR_LOG_FMT").Short('f').Default("logfmt").Envar("VGVSR_LOG_FMT").EnumVar(&logFormat, "logfmt", "json")

	// Now parse the flags
	kingpin.MustParse(app.Parse(os.Args[1:]))

	lug.InitLogger(logFormat, logLevel, logUTC)

	tradDNS = TradDNS.NewTradDNS()
	kubeAPI = KubeAPI.NewKubeAPI()
	gdns = GDNS.NewGDNS(ctx, kubeAPI)

	initCert()
}

// Pulls in or creates the certificate and key.
func initCert() {
	//TODO: Override these variables with command line flags
	// Attempt to fecth the private key and certificate from environment variables.
	keyPEMStr := os.Getenv("VGVSR_PRIVATE_KEY")
	certPEMStr := os.Getenv("VGVSR_CERTIFICATE")

	// If either of those are empty
	if keyPEMStr == "" || certPEMStr == "" {
		if keyPEMStr == "" {
			lug.LogWarn("Message", "Failed to find key")
		}
		if certPEMStr == "" {
			lug.LogWarn("Message", "Failed to find Certificate")
		}
		lug.LogWarn("Message", "Generating key and self-signed certificate")
		// Generate a default key and cert
		privKey, pubKey := genKeyPair(2048)

		keyPEM = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
		certPEM = genCert(privKey, pubKey)
	} else {
		lug.LogWarn("Message", "Certificate and key found")
		pemStr, err := base64.StdEncoding.DecodeString(keyPEMStr)
		if err != nil {
			lug.LogError("Message", "Failed", "Error", err)
		}
		certStr, err := base64.StdEncoding.DecodeString(certPEMStr)
		if err != nil {
			lug.LogError("Message", "Failed", "Error", err)
		}
		keyPEM = []byte(pemStr)
		certPEM = []byte(certStr)
	}

}

// Creates and returns reference a gorillamux router
func getRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", mainHandler).Methods("GET")
	router.HandleFunc("/health", healthHandler)

	// Not sure how to enable metrics when I'm doing them the way I'm doing them.
	router.Handle("/metrics", promhttp.Handler())
	// Ashish did it this way. I should figure out if there's a benefit.
	//	promhttp.HandlerFor(r, promhttp.HandlerOpts{ErrorHandling: promhttp.ContinueOnError})

	router.HandleFunc("/dns", tradDNS.GetDNSRecords).Methods("GET")
	router.HandleFunc("/dns/{id}", tradDNS.GetDNSRecord).Methods("GET")
	router.HandleFunc("/dns/{id}", tradDNS.CreateDNSRecord).Methods("POST")
	router.HandleFunc("/dns/{id}", tradDNS.DeleteDNSRecord).Methods("DELETE")


	router.HandleFunc("/k8s", kubeAPI.GetK8SRecords).Methods("GET")
	router.HandleFunc("/k8s/{id}", kubeAPI.GetK8SRecord).Methods("GET")
	router.HandleFunc("/k8s", kubeAPI.CreateK8SRecord).Methods("POST")
	router.HandleFunc("/k8s/{id}", kubeAPI.DeleteK8SRecord).Methods("DELETE")

	// I should probably not have to call this one by hand.
	router.HandleFunc("/k8s-update", updateClustersHandler).Methods("GET")

	router.HandleFunc("/gdns", gdns.GetGDNSRecords).Methods("GET")
	router.HandleFunc("/gdns/{id}", gdns.GetGDNSRecord).Methods("GET")
	router.HandleFunc("/gdns", gdns.CreateGDNSRecord).Methods("POST")
	router.HandleFunc("/gdns/{id}", gdns.DeleteGDNSRecord).Methods("DELETE")

	return router
}

// Enables cross-site script calls.
func setCORS(w http.ResponseWriter) {
	for h, v := range corsHeaders {
		w.Header().Set(h, v)
	}
}

// Triggers the creation of k8s clients and tests the server.
func updateClustersHandler(w http.ResponseWriter, r *http.Request) {
	// Log the request to STDOUT
	lug.LogWarn("Message", "updateClustersHandler called")
	// Set CSS headers
	//setCORS(w)

	kubeAPI.UpdateClusters(ctx)

	// Respond with a 200
	w.WriteHeader(http.StatusOK)
	message := fmt.Sprintf("{\n\"Message\": \"Updated\"\n")
	io.WriteString(w, message)
}

// A main handler which lug.Logs when it was accessed and responds with "Sample App"
func mainHandler(w http.ResponseWriter, r *http.Request) {
	// Log the request to STDOUT

	lug.LogWarn("Message", "/ called at %s\n", time.Now())
	// Set CSS headers
	setCORS(w)

	// Respond with a 200
	w.WriteHeader(http.StatusOK)
	message := fmt.Sprintf("Nothing here, perhaps you meant <a href=\"/gdns\"")
	io.WriteString(w, message)
}

func main() {
	// Initialize the prometheus counters and the /metrics path
	metricsArray = Metrics.NewVerifyMetrics()
	metricsArray.RegisterAllMetrics()


	// Configure TLS
	cer, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		lug.LogError("Message", "Failed", "Error", err)
	}
	config := &tls.Config{
		MinVersion: tls.VersionTLS12,
		Certificates: []tls.Certificate{cer},
	}
	listener, err := tls.Listen("tcp", fmt.Sprintf(":%d", apiport), config)
	if err != nil {
		lug.LogError("Message", "Failed", "Error", err)
	}

	server := http.Server{
		Handler:		getRouter(),
		TLSConfig:		config,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
	}

	// Create some bogus Trad DNS records.
	createDefaultTradDNSRecords()

	// Create some bogus Kubernetes clusters.
	createDefaultAPIServers()

	// Create some bogus GlobalDNS records.
	createDefaultGDNSRecords()

	lug.LogInfo("Message", "Registering DNS Handler")
	// attach request handler func
	dns.HandleFunc(TheZone.Origin, handleDnsRequest)

	// Create DNS server
	DNSServer := &dns.Server{Addr: ":" + strconv.Itoa(dnsport), Net: "udp"}

	var DNSerr error

	go func() {
		lug.LogWarn("Message", "Starting DNS Service", "Port", dnsport)
		DNSerr = DNSServer.ListenAndServe()
		defer DNSServer.Shutdown()
	}()

	if DNSerr != nil {
		lug.LogError("Message", "Failed to start DNS server", "Error", err.Error())
	}

	//TODO: Do something smarter here (will have to understand go's native context a bit more for that.)
	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(context.Background())
	defer cancel() // cancel when we are finished

	//http.ServeTLS(listener, router, getMux(versionInfo))
	lug.LogError("Message", "Application startup", "BaseVersion", BaseVersion, "APIPort", apiport, "DNSPort", dnsport)

	fmt.Printf("Failed with error %v", server.Serve(listener))
}
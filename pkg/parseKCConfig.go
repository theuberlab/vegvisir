package main
// This is a stupid hack to correct for a couple of issues loading the kubectl config file.
//
// This issue:
// https://github.com/ericchiang/k8s/issues/81
//
// Where we cannot unmarshal the kubectl config file due to an issue with the format of certificate-authority-data
//
// yaml: unmarshal errors:
//  line 6: cannot unmarshal !!str `LS0tLS1...` into []uint8
//
// Also modifies implicit paths to be explicit paths

import (
	"bitbucket.org/theuberlab/vegvisir/pkg/lug"
	"fmt"
	"io/ioutil"
	"os/user"
	"os"
	"path/filepath"
	"strings"

	"github.com/ericchiang/k8s"
	//"gopkg.in/yaml.v2"
	"github.com/ghodss/yaml"
)

// Our own struct for the config file so we can pre-parse it and screw with it.
type kubeConfigFile struct {
	APIVersion string `yaml:"apiVersion"`
	Clusters  *[]clusterEntry `yaml:"clusters,omitempty"`
	Contexts []struct {
		Context struct {
			Cluster   string `yaml:"cluster"`
			Namespace string `yaml:"namespace,omitempty"`
			User      string `yaml:"user"`
		} `yaml:"context"`
		Name string `yaml:"name"`
	} `yaml:"contexts,omitempty"`
	CurrentContext string `yaml:"current-context"`
	Kind           string `yaml:"kind,omitempty"`
	Preferences    struct {
	} `yaml:"preferences,omitempty"`
	Users []struct {
		Name string `yaml:"name"`
		User struct {
			Token string `yaml:"token"`
		} `yaml:"user"`
	} `yaml:"users,omitempty"`
}

type clusterEntry struct {
	Cluster struct {
		CertificateAuthority string `yaml:"certificate-authority,omitempty"`
		CertificateAuthorityData string `yaml:"certificate-authority-data,omitempty"`
		Server               string `yaml:"server"`
	} `yaml:"cluster"`
	Name string `yaml:"name"`
}

// Figures out which config file to use.
func findConfigFile(configFile string) (string, string, error) {
	var relativePathPrefix string // prepend this to relative paths in the config file so that ericchiang/k8s can handle it.

	// If configFile is empty use the default
	if configFile == "" {
		configFile = "config"
	}

	// Check if the config file is an explicit or implicit path.
	if strings.HasPrefix(configFile, "/") {
		// Explicit path do nothing
	} else if strings.HasPrefix(configFile, ".") {
		// Implicit path
		// Append the path to the file to $CWD then use that as the prefix.
		cwd, err := os.Getwd()
		if err != nil {
			return "", "", fmt.Errorf("Cannot get user home directory err: %v\n", err)
		}

		configFile = filepath.Join(cwd, configFile)
	} else {
		// Only a file name prepend $home/.kube
		usr, err := user.Current()
		if err != nil {
			return "", "", fmt.Errorf("Cannot get user home directory err: %v\n", err)
		}

		configFile = filepath.Join(usr.HomeDir, "/.kube/", configFile)
	}

	relativePathPrefix = filepath.Dir(configFile)
	return configFile, relativePathPrefix, nil
}

// Pulls the config file in an parses it.
func loadConfigFile(configFilePath string) ([]byte, error) {
	data, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, fmt.Errorf("read kubeconfig: %v", err)
	}
	return data, nil
}

// Loads a config file, removes unparsable sections and modifies relative paths.
func loadNormalizedConfigFile(configFilePath string, relativePathPrefix string) (*k8s.Config, error) {
	data, err := loadConfigFile(configFilePath)
	if err != nil {
		return &k8s.Config{}, err
	}

	// Screw with the config object

	// Unmarshal YAML into OUR Kubernetes config object so we can screw with it..
	var config kubeConfigFile
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, fmt.Errorf("unmarshal kubeconfig: %v", err)
	}

	// store the names of the clusters we are going to pull
	var clustersToRemove []string

	var trimmedClusters []clusterEntry

	// If the context has been specified modify the config we load to use that one.
	if configContext != "" && configContext != "default" {
		lug.LogError("msg", "Using specified context.", "ConfigContext", configContext)
		config.CurrentContext = configContext
	} else {
		lug.LogDebug("msg", "Using default context.")
	}
	
	// Loop through clusters and pull out any where the certificate-authority will break us.
	for _, value := range *config.Clusters {

		if strings.Contains(value.Cluster.CertificateAuthorityData, "LS0tLS1") {
			lug.LogInfo("msg", "Cluster has an unreadable certificate-authority-data field",
				"ClusterName", value.Name,
				"CertificateAuthorityDataValue", value.Cluster.CertificateAuthorityData)

			clustersToRemove = append(clustersToRemove, value.Name)
		} else {
			trimmedClusters = append(trimmedClusters, value)
		}

		lug.LogDebug("msg", "Cluster Config", "ClusterName", value.Name,
			"CertificateAuthorityFileValue", value.Cluster.CertificateAuthority,
			"CertificateAuthorityDataValue", value.Cluster.CertificateAuthorityData)
	}


	// Loop through what's left and correct for relative paths
	for index, value := range trimmedClusters {
		if strings.Compare(value.Cluster.CertificateAuthority, "") != 0 && !strings.HasPrefix(value.Cluster.CertificateAuthority, "/") {
			newAuthority := filepath.Join(relativePathPrefix, value.Cluster.CertificateAuthority)

			lug.LogInfo("msg", "Correcting certificate-authority",
				"FromValue", value.Cluster.CertificateAuthority,
				"ToValue", newAuthority)

			trimmedClusters[index].Cluster.CertificateAuthority = newAuthority

			lug.LogDebug("msg", "CA Value", "CAFileValue", value.Cluster.CertificateAuthority)
		} else {
			lug.LogDebug("msg", "No CertificateAuthority For Cluster: Skipping", "ClusterName", value.Name)

		}
	}

	// Set the list to only the ones we've decied to retain.
	config.Clusters = &trimmedClusters

	//TODO: This _probably_ isn't neccessary just to get this working properly but it might shorten processing a tiny bit if we loop through contexts and remove references to the clusters we've removed.

	// re-marshal to yaml and back to k8s.config
	var k8sonfig k8s.Config

	correctedYaml, err := yaml.Marshal(config)
	if err != nil {
		return nil, fmt.Errorf("Error remarshaling kubeconfig: %v", err)
	}

	// Unmarshal back to a Kubernetes config object.
	err = yaml.Unmarshal(correctedYaml, &k8sonfig)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshaling corrected kubeconfig: %v", err)
	}

	return &k8sonfig, nil
}

// loadClient parses a kubeconfig from a file and returns a Kubernetes
// client. It does not support extensions or client auth providers.
func loadClient(kubeconfigPath string) (*k8s.Client, error) {
	kubeconfigPath, relativePathPrefix, err := findConfigFile(kubeconfigPath)

	if err != nil {
		return nil, fmt.Errorf("Unable to load kubectl config: %v", err)
	}

	normalizedConfigFile, err := loadNormalizedConfigFile(kubeconfigPath, relativePathPrefix)
	if err != nil {
		return nil, fmt.Errorf("Unable to load kubectl config: %v", err)
	}

	return k8s.NewClient(normalizedConfigFile)
}
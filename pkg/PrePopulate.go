package main

import (
	"bitbucket.org/theuberlab/vegvisir/pkg/GDNS"
	"bitbucket.org/theuberlab/vegvisir/pkg/KubeAPI"
	"bitbucket.org/theuberlab/vegvisir/pkg/TradDNS"
)

// Pre-populate a few different types of records.

// Creates some example TradDNS records.
func createDefaultTradDNSRecords() {
	tradDNS.DNSRecords["www.example.com."] = &TradDNS.ResourceRecord{
		Zone:			TheZone,
		HostLabel:		"www",
		TTL:			0,
		RecordClass:	TradDNS.IN,
		RecordType:		TradDNS.A,
		RecordData:		"10.1.2.3",
	}

	tradDNS.DNSRecords["email.example.com."] = &TradDNS.ResourceRecord{
		Zone:			TheZone,
		HostLabel:		"email",
		TTL:			0,
		RecordClass:	TradDNS.IN,
		RecordType:		TradDNS.A,
		RecordData:		"10.1.2.4",
	}
}

// Creates some example Kubernetes API servers
func createDefaultAPIServers() {
	kubeAPI.APIServers["bogus-kube-cluster-1"] = &KubeAPI.KubeAPIServer{
		Name:		"bogus-kube-cluster-1",
		URL:		"https://bogus-kube-cluster-1.example.com",
		CAPem:		"The certificate used to validate the API goes here.",
		AuthToken:	"An authentication token with the ability to read services goes here.",
	}
}

// Create some example GDNS records.
func createDefaultGDNSRecords() {
	gdns.GDNSRecords["bogusgdnsrecord"] = &GDNS.GdnsRecord{
		Name: "bogusgdnsrecord",
		Zone: TheZone,
		Services: []GDNS.ServiceAddress{
			{
				Name:				"someapp",
				InternalEndpoint:	"25.0.0.5",
				ExternalEndpoint:	"1.2.3.4",
				Namespace:			"some-ns",
				Selector:			GDNS.Selector {
					Method: 			"Eq",
					Key: 				"app",
					Value: 				"some-app",
				},
				Cluster:			"bogusCluster1",
				Status:				true,
			},
		},
	}
}
// Example originally from https://gist.github.com/walm/0d67b4fb2d5daf3edd4fad3e13b162cb
//
package main

import (
	"fmt"
	"log"

	"github.com/miekg/dns"
)

// Parses the DNS request determining the type. Will later parse out request options.
func parseQuery(message *dns.Msg) {
	for _, question := range message.Question {
		log.Printf("Query for %s\n", question.Name)
		switch question.Qtype {
		case dns.TypeA:
			var ip string


			if gdns.GDNSRecords[question.Name] != nil {
				for _, service := range gdns.GDNSRecords[question.Name].Services {
					ip = service.ExternalEndpoint
					rr, err := dns.NewRR(fmt.Sprintf("%s CNAME %s", question.Name, ip))
					if err == nil {
						message.Answer = append(message.Answer, rr)
					}
				}
			} else {
				ip = tradDNS.DNSRecords[question.Name].RecordData
				if ip != "" {
					rr, err := dns.NewRR(fmt.Sprintf("%s A %s", question.Name, ip))
					if err == nil {
						message.Answer = append(message.Answer, rr)
					}
				}
			}

		//TODO: Respond with a no such record if it isn't found in either place.
		default:
			log.Printf("Unknown question type: %v", question.Qtype)
			message.Opcode = dns.RcodeNotImplemented

		}

	}
}

func handleDnsRequest(Writer dns.ResponseWriter, request *dns.Msg) {
	log.Printf("Processing DNS request: %v\n", request)
	message := new(dns.Msg)
	message.SetReply(request)
	message.Compress = false

	switch request.Opcode {
	case dns.OpcodeQuery:
		parseQuery(message)
	default:
		message.Opcode = dns.RcodeNotImplemented
	}

	Writer.WriteMsg(message)
}

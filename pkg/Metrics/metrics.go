package Metrics

import "github.com/prometheus/client_golang/prometheus"

const (
	metricsNamespace			= "vegvisir"
	healthCheckRequests			= "health_check_num_requests"
	healthCheckRequests_help	= "How many times the helthcheck path has been requested. This is an example metric which would probably be silly in production and is likely to be removed."
)

// VerifyMetrics contains verification Metrics
type VerifyMetrics struct {
	Metrics map[string]prometheus.Collector
}

//NewVerifyMetrics returns new verify Metrics
func NewVerifyMetrics() *VerifyMetrics {
	return &VerifyMetrics{
		Metrics: map[string]prometheus.Collector{
			healthCheckRequests: prometheus.NewCounterVec(
				prometheus.CounterOpts{
					Namespace: metricsNamespace,
					Subsystem: "health",
					Name:		healthCheckRequests,
					Help:		healthCheckRequests_help,
				},
				[]string{"method", "code"},
			),
		},
	}
}

// RegisterAllMetrics registers all Metrics that represent verification result
func (v *VerifyMetrics) RegisterAllMetrics() {
	for _, vm := range v.Metrics {
		prometheus.MustRegister(vm)
	}
}

// Create functions here for incrementing the various metrics and they can be called by the various endpoints.
// Increments the helth check counter
func (v *VerifyMetrics) IncrementHealthCheckCounter(method string, code string) {
	if s, ok := v.Metrics[healthCheckRequests].(*prometheus.CounterVec); ok {
		s.With(prometheus.Labels{"method": method, "code": code}).Inc()
	}
}



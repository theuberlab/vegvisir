package main

var BaseVersion string = "0.2"

//TODO: Start returning version as an object instead of just a string
// Something like this:
type VersionInfo struct {
	Major		int
	Minor		int
	Patch		int
	Build		string
}

func (v VersionInfo) GetVersion() string {
	return BaseVersion
}
